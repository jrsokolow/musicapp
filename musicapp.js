if (Meteor.isClient) {

  Meteor.startup(function () {
    if(this.userId) {
      $('#form').removeAttr('style');
    }
  });
  
  Music = new Meteor.Collection("music");

  Accounts.config({
    forbidClientAccountCreation:true
  });

  Template.form.events({
    "click input[type='button']" : function () {
      // template data, if any, is available in 'this'
      if (typeof console !== 'undefined') {
        var videoUrl = $('#url').val();
        var videoId = getVideoId(videoUrl);
        var videoEmbeddedUrl = getYouTubeVideoEmbededUrl(videoId);
        saveSong(videoId, videoEmbeddedUrl);
      }
    }
  });

  function getVideoId(videoUrl) {
    var video_id = videoUrl.split('v=')[1];
    var ampersandPosition = video_id.indexOf('&');
    if(ampersandPosition != -1) {
      video_id = video_id.substring(0, ampersandPosition);
    }
    return video_id
  }

  function getYouTubeVideoEmbededUrl(videoId) {
    return 'http://www.youtube.com/embed/' + videoId + '?autoplay=1';
  }

  function saveSong(videoId, videoEmbeddedUrl) {
    var service = 'https://gdata.youtube.com/feeds/api/videos/' + videoId + '?v=2&alt=jsonc'
    $.getJSON(service, function(result) {
      console.log(result.data);
      Music.insert({url:videoEmbeddedUrl, title:result.data.title, thumbnail:result.data.thumbnail.hqDefault}); 
      $('#url').val('');
    }); 
  }

  Template.list.songs = function() {
    return Music.find().fetch();
  };

  Template.list.removeSong = function(songUrl) {
    Music.remove({url:songUrl});
  };

  Template.list.playSong = function(songUrl) {
    var player = "<iframe title='YouTube video player' class='youtube-player' style='border-style:solid;border-width:5px;' type='text/html' width='340' height='250' src='" + songUrl + "' frameborder='0' allowFullScreen scrolling='no' />";
    $("div[id='player']").html(player);
  };

  Template.login.events({
    "click div[id='login-buttons-password']": function(event) {
        $('#form').removeAttr('style');
    },
    "click div[id='login-buttons-logout']": function(event) {
        $('#form').attr('style', 'display:none');
    }
  });

}

if (Meteor.isServer) {
  Meteor.startup(function () {});
  Music = new Meteor.Collection("music");
  Meteor.startup(function () {
    if (Meteor.users.find().count() === 0) {
      Accounts.createUser({username: "Jakub Sokolowski", email:'jrsokolow@gmail.com', password:'Prosiaczek83'});
    }
  });
}
